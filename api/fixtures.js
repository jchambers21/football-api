// Import Dependencies
const url = require("url");
const MongoClient = require("mongodb").MongoClient;

// Create cached connection variable
let cachedDb = null;

// A function for connecting to MongoDB,
// taking a single parameter of the connection string
async function connectToDatabase(uri) {
  // If the database connection is cached,
  // use it instead of creating a new connection
  if (cachedDb) {
    return cachedDb;
  }

  // If no connection is cached, create a new one
  const client = await MongoClient.connect(uri, { useNewUrlParser: true });

  // Select the database through the connection,
  // using the database path of the connection string
  const db = await client.db(url.parse(uri).pathname.substr(1));

  // Cache the database connection and return the connection
  cachedDb = db;
  return db;
}

// The main, exported, function of the endpoint,
// dealing with the request and subsequent response
module.exports = async (req, res) => {
  // Get a database connection, cached or otherwise,
  // using the connection string environment variable as the argument
  const db = await connectToDatabase(process.env.MONGODB_URI);

  // Select the "users" collection from the database
  const collection = await db.collection("fixtures");

  let match = {};
  if (req.query.date) {
    match.event_date = {
      $gte: new Date(`${req.query.date}T00:00:00.000+00:00`),
      $lt: new Date(`${req.query.date}T23:59:00.000+00:00`),
    };
  }

  if (req.query.status) {
    match.statusShort = { $in: req.query.status.split("-") };
  }

  if (req.query.fixture_id) {
    match.fixture_id = Number(req.query.fixture_id);
  }

  // Select the users collection from the database
  const fixtures = await collection
    .aggregate([
      {
        $match: match,
      },
      {
        $lookup: {
          from: "leagues", //Get data from values table
          localField: "league_id", //The field _id of the current table uniques
          foreignField: "league_id", //The foreign column containing a matching value
          as: "related", //An array containing all items under 69
        },
      },
      {
        $unwind: "$related",
      },
      {
        $group: {
          _id: "$related._id",
          league: {
            $first: {
              _id: "$related._id",
              disorder: "$related.disorder",
              name: "$related.name",
              country: "$related.country",
            },
          },
          fixtures: {
            $push: {
              fixture_id: "$fixture_id",
              round: "$round",
              statusShort: "$statusShort",
              elapsed: "$elapsed",
              homeTeam: "$homeTeam",
              awayTeam: "$awayTeam",
              goalsHomeTeam: "$goalsHomeTeam",
              goalsAwayTeam: "$goalsAwayTeam",
              score: "$score",
              event_date: "$event_date",
              events: "$events",
            },
          },
        },
      },
      { $sort: { "league.disorder": 1 } },
    ])
    .toArray();

  // Respond with a JSON string of all users in the collection
  res.status(200).json({ fixtures });
};
