// Import Dependencies
const url = require("url");
const MongoClient = require("mongodb").MongoClient;

// Create cached connection variable
let cachedDb = null;

// A function for connecting to MongoDB,
// taking a single parameter of the connection string
async function connectToDatabase(uri) {
  // If the database connection is cached,
  // use it instead of creating a new connection
  if (cachedDb) {
    return cachedDb;
  }

  // If no connection is cached, create a new one
  const client = await MongoClient.connect(uri, { useNewUrlParser: true });

  // Select the database through the connection,
  // using the database path of the connection string
  const db = await client.db(url.parse(uri).pathname.substr(1));

  // Cache the database connection and return the connection
  cachedDb = db;
  return db;
}

// The main, exported, function of the endpoint,
// dealing with the request and subsequent response
module.exports = async (req, res) => {
  // Get a database connection, cached or otherwise,
  // using the connection string environment variable as the argument
  const db = await connectToDatabase(process.env.MONGODB_URI);

  // Select the "users" collection from the database
  const collection = await db.collection("leagues");

  let match = {};
  if (req.query.standings) {
    match.standings = Number(req.query.standings);
  }
  if (req.query.current) {
    match.is_current = Number(req.query.current);
  }
  if (req.query.name_id) {
    match.name_id = req.query.name_id;
  }

  // Select the users collection from the database
  const leagues = await collection
    .find( match )
    .sort({ disorder: 1 })
    .toArray();

  // Respond with a JSON string of all users in the collection
  res.status(200).json({ leagues });
};
